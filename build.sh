#!/bin/bash

OUTPUT=`pwd`/output

set -x
set -e

docker pull bhdouglass/linphone-compile:xenial

rm -rf $OUTPUT
mkdir $OUTPUT

echo "Compiling libsrtp"
cd libsrtp
../run-docker.sh make distclean || true
../run-docker.sh ./configure --prefix=$OUTPUT --host=arm-linux --with-gnu-ld --enable-log-stdout
../run-docker.sh make
# Don't compile the shared lib, it crashes everything, but for some reason the static is perfectly fine
#../run-docker.sh make shared_library
../run-docker.sh make install
cd ..

echo "Compiling Speexdsp"
cd speexdsp
../run-docker.sh make clean || true
../run-docker.sh ./autogen.sh
../run-docker.sh ./configure --prefix=$OUTPUT --host=arm-linux --with-gnu-ld --disable-static --enable-fixed-point --enable-neon --disable-examples
../run-docker.sh make install
cd ..

echo "Compiling Speex"
cd speex
../run-docker.sh make clean || true
../run-docker.sh ./autogen.sh
../run-docker.sh ./configure --prefix=$OUTPUT --host=arm-linux --with-gnu-ld --disable-static --enable-fixed-point --disable-binaries
../run-docker.sh make install
cd ..

echo "Compiling Opus"
cd opus
../run-docker.sh make clean || true
../run-docker.sh ./autogen.sh
../run-docker.sh ./configure --prefix=$OUTPUT --host=arm-linux --enable-fixed-point --enable-fuzzing --disable-doc --disable-extra-programs
../run-docker.sh make install
cd ..

echo "Compiling bcunit"
cd bcunit
../run-docker.sh make clean || true
../run-docker.sh cmake . -DCMAKE_INSTALL_PREFIX=$OUTPUT
../run-docker.sh make install
cd ..

echo "Compiling bctoolbox"
cd bctoolbox
../run-docker.sh make clean || true
../run-docker.sh cmake . -DCMAKE_INSTALL_PREFIX=$OUTPUT -DENABLE_MBEDTLS=YES -DENABLE_POLARSSL=NO -DENABLE_DECAF=NO -DENABLE_TESTS_COMPONENT=NO
../run-docker.sh make install
cd ..

echo "Compiling bzrtp"
cd bzrtp
../run-docker.sh make clean || true
../run-docker.sh cmake . -DCMAKE_INSTALL_PREFIX=$OUTPUT
../run-docker.sh make install
cd ..

echo "Compiling ortp"
cd ortp
../run-docker.sh make clean || true
../run-docker.sh cmake . -DCMAKE_INSTALL_PREFIX=$OUTPUT -DENABLE_DOC=NO -DENABLE_PERF=YES  # Enabling PERF makes a huge difference in lag!
../run-docker.sh make install
cd ..

echo "Compiling belle-sip"
cd belle-sip
../run-docker.sh make clean || true
../run-docker.sh cmake . -DCMAKE_INSTALL_PREFIX=$OUTPUT -DENABLE_TESTS=NO
../run-docker.sh make install
cd ..

echo "Compiling belr"
cd belr
../run-docker.sh make clean || true
../run-docker.sh cmake . -DCMAKE_INSTALL_PREFIX=$OUTPUT -DENABLE_TOOLS=NO -DENABLE_UNIT_TESTS=NO
../run-docker.sh make install
cd ..

echo "Compiling mediastreamer2"
cd mediastreamer2
../run-docker.sh make clean || true
../run-docker.sh cmake . -DCMAKE_INSTALL_PREFIX=$OUTPUT -DENABLE_VIDEO=NO -DENABLE_TOOLS=NO -DENABLE_UNIT_TESTS=NO -DENABLE_ALSA=NO -DENABLE_SRTP=YES -DENABLE_ZRTP=YES -DENABLE_RELATIVE_PREFIX=YES -DENABLE_MKV=NO -DENABLE_JPEG=NO
../run-docker.sh make install
cd ..

#TODO figure out if there is a proper way to do this
mv $OUTPUT/lib/lib* $OUTPUT/lib/arm-linux-gnueabihf/

echo "Compiling linphone"
cd linphone
../run-docker.sh make clean || true
../run-docker.sh ../build-linphone.sh
cd ..

echo "Copying extra libs"
./run-docker.sh bash -c "cp /usr/lib/arm-linux-gnueabihf/libantlr3c-3.2.so* output/lib/arm-linux-gnueabihf"
./run-docker.sh bash -c "cp /usr/lib/arm-linux-gnueabihf/libmbedx509.so* output/lib/arm-linux-gnueabihf"
./run-docker.sh bash -c "cp /usr/lib/arm-linux-gnueabihf/libmbedcrypto.so* output/lib/arm-linux-gnueabihf"

echo "Cleaning output directoy"
./cleanup.sh
